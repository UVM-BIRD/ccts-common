# CCTS Common #

CCTS Common is a set of common libraries used in projects developed at the University of Vermont Center for Clinical and Translational Science.

### Usage ###

Including the CCTS Common library in your project can be accomplished by including the following dependency in your project's Maven POM:

    <dependency>
        <groupId>edu.uvm.ccts</groupId>
        <artifactId>ccts-common</artifactId>
        <version>1.0</version>
    </dependency>

### Building CCTS Common ###

Building the CCTS Common library and making it available to other local Maven projects is accomplished by running the following:

    $ mvn package
    $ mvn install

### License and Copyright ###

The CCTS Common library is Copyright 2015 [The University of Vermont and State Agricultural College](https://www.uvm.edu/).  All rights reserved.

The CCTS Common library is licensed under the terms of the [GNU General Public License (GPL) version 3](https://www.gnu.org/licenses/gpl-3.0-standalone.html).
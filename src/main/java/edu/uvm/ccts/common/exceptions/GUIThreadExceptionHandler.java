/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.exceptions;

import edu.uvm.ccts.common.ui.ShowException;

import java.awt.*;

/**
 * Created by mstorer on 8/29/14.
 */
public class GUIThreadExceptionHandler implements Thread.UncaughtExceptionHandler {
    private Component owner;
    private String title;

    public GUIThreadExceptionHandler(Frame frame, String title) {
        owner = frame;
        this.title = title;
    }

    public GUIThreadExceptionHandler(Dialog dialog, String title) {
        owner = dialog;
        this.title = title;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        ShowException se;

        if (owner instanceof Frame) {
            se = new ShowException((Frame) owner, title, e);

        } else if (owner instanceof Dialog) {
            se = new ShowException((Dialog) owner, title, e);

        } else {
            throw new UnhandledClassException("unhandled class : " + owner.getClass().getName());
        }

        se.setVisible(true);
    }
}

/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.exceptions.antlr;

/**
 * Created by mstorer on 10/2/13.
 */
public class AntlrException extends RuntimeException {
    private Type type;

    public AntlrException(Type type) {
        super();
        this.type = type;
    }

    public AntlrException(Type type, Exception e) {
        super(e);
        this.type = type;
    }

    public AntlrException(Type type, String message) {
        super(message);
        this.type = type;
    }

    public AntlrException(Type type, String message, Exception e) {
        super(message, e);
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public static enum Type {
        PARSER,
        LEXER
    }
}

/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.mail;

import edu.uvm.ccts.common.exceptions.BadCredentialsException;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.model.AbstractCredentialedObject;
import electric.xml.Element;
import electric.xml.XPath;

import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;
import java.awt.*;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by mstorer on 1/16/14.
 */
public class MailConfig extends AbstractCredentialedObject implements Serializable {
    private String smtpHost;
    private int smtpPort;
    private boolean useTLS = false;
    private String user;
    private String password;
    private String fromAddress;

    public static MailConfig buildFromXml(Element xmlMailConfig) throws IOException, ConfigurationException {
        return buildFromXml(xmlMailConfig, null);
    }

    public static MailConfig buildFromXml(Element xmlMailConfig, Frame frame) throws IOException, ConfigurationException {
        Element xmlSmtp = xmlMailConfig.getElement(new XPath("smtp"));
        Element xmlSender = xmlMailConfig.getElement(new XPath("sender"));

        boolean useTLS = xmlSmtp.hasAttribute("security") && xmlSmtp.getAttribute("security").equalsIgnoreCase("tls");

        String host = xmlSmtp.getAttribute("host");
        int port = Integer.parseInt(xmlSmtp.getAttribute("port"));
        String user = xmlSmtp.getAttribute("user");
        String password = xmlSmtp.getAttribute("password");
        String email = xmlSender.getAttribute("email");

        return new MailConfig(host, port, useTLS, user, password, email, frame);
    }


////////////////////////////////////////////////////////////////////////////////////////


    public MailConfig(String smtpHost, int smtpPort, boolean useTLS, String user, String password, String fromAddress)
            throws ConfigurationException {

        this(smtpHost, smtpPort, useTLS, user, password, fromAddress, null);
    }

    public MailConfig(String smtpHost, int smtpPort, boolean useTLS, String user, String password, String fromAddress, Frame frame)
            throws ConfigurationException {

        this.smtpHost = smtpHost;
        this.smtpPort = smtpPort;
        this.useTLS = useTLS;
        this.user = user;
        this.password = password;
        this.fromAddress = fromAddress;

        ensureCredentials(frame);
    }

    public String getSmtpHost() {
        return smtpHost;
    }

    public int getSmtpPort() {
        return smtpPort;
    }

    public boolean isUseTLS() {
        return useTLS;
    }

    @Override
    public String getUser() {
        return user;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public String getFromAddress() {
        return fromAddress;
    }


/////////////////////////////////////////////////////////////////////////////////////////


    @Override
    protected String getCredentialType() {
        return "SMTP host";
    }

    @Override
    protected String getCredentialTarget() {
        return smtpHost;
    }

    @Override
    protected void setUser(String user) {
        this.user = user;
    }

    @Override
    protected void setPassword(String password) {
        this.password = password;
    }

    @Override
    protected void testCredentials() throws BadCredentialsException, ConfigurationException {
        try {
            MailUtil.testConnection(this);

        } catch (AuthenticationFailedException afe) {
            throw new BadCredentialsException(afe.getMessage(), afe);

        } catch (MessagingException me) {
            throw new ConfigurationException(me.getMessage(), me);
        }
    }
}

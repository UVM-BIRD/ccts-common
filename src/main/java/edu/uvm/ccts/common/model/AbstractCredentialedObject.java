/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.model;

import edu.uvm.ccts.common.exceptions.BadCredentialsException;
import edu.uvm.ccts.common.exceptions.CanceledException;
import edu.uvm.ccts.common.exceptions.ConfigurationException;
import edu.uvm.ccts.common.ui.GetCredentials;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.*;
import java.io.*;

/**
 * Created by mstorer on 10/21/14.
 */
public abstract class AbstractCredentialedObject implements Serializable {
    private static final Log log = LogFactory.getLog(AbstractCredentialedObject.class);

    protected abstract String getCredentialType();
    protected abstract String getCredentialTarget();
    protected abstract String getUser();
    protected abstract String getPassword();
    protected abstract void setUser(String user);
    protected abstract void setPassword(String password);
    protected abstract void testCredentials() throws BadCredentialsException, ConfigurationException;

    protected void ensureCredentials(Frame frame) throws ConfigurationException {
        ensureCredentials(null, frame);
    }

    protected void ensureCredentials(String note, Frame frame) throws ConfigurationException {
        boolean success = false;
        String message = null;
        boolean usernameHardcoded = getUser() != null;

        while ( ! success ) {
            getCredentials(note, message, frame);

            try {
                testCredentials();
                success = true;

            } catch (BadCredentialsException e) {
                log.error("user provided invalid credentials for " + getCredentialType() + " '" + getCredentialTarget() +
                        "' - " + e.getMessage());

                if ( ! usernameHardcoded ) setUser(null);
                setPassword(null);
                message = e.getMessage();
            }
        }
    }

    private void getCredentials(String note, String message, Frame frame) {
        try {
            if (frame != null) {
                if (getUser() == null || getPassword() == null) {
                    String prompt = "Please provide authentication credentials for " + getCredentialType() + " '" + getCredentialTarget() + "':";
                    GetCredentials frmCred = new GetCredentials(frame, prompt, note, getUser(), message);

                    frmCred.setVisible(true);

                    if (frmCred.getCloseAction() == GetCredentials.CloseAction.CANCEL) {
                        throw new CanceledException();
                    }

                    setUser(frmCred.getUser());
                    setPassword(frmCred.getPassword());
                }

            } else {
                if (getUser() == null) {
                    if (message != null) System.out.println("*** " + message);
                    setUser(getUsernameFromUser());
                    setPassword(getPasswordFromUser());

                } else if (getPassword() == null) {
                    if (message != null) System.out.println("*** " + message);
                    setPassword(getPasswordFromUser());
                }
            }

        } catch (IOException e) {
            log.error("caught " + e.getClass().getName() + " getting credentials - " + e.getMessage(), e);
        }
    }

    private String getUsernameFromUser() throws IOException {
        Console con = System.console();
        String user = "";
        String prompt = "Enter username for " + getCredentialType() + " '" + getCredentialTarget() + "': ";

        while (user.equals("")) {
            if (con != null) {
                user = System.console().readLine(prompt).trim();

            } else {
                System.out.print(prompt);
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                user = reader.readLine().trim();
            }

            if (user.equals("")) System.out.println("Invalid username.");
        }

        return user;
    }

    private String getPasswordFromUser() throws IOException {
        Console con = System.console();
        String prompt = "Enter password for User '" + getUser() + "' at " + getCredentialType() + " '" + getCredentialTarget() + "': ";

        if (con != null) {
            char[] pass = System.console().readPassword(prompt);
            return new String(pass);

        } else {
            System.out.print(prompt);
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            return reader.readLine().trim();
        }
    }
}

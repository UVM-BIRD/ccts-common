/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.File;
import java.io.IOException;

/**
 * Created by mstorer on 11/26/13.
 */
public class FileMetadata {
    private static final String delim = ",";

    private String filename;
    private long timestamp;
    private long size;

    public FileMetadata(File file) throws IOException {
        this.filename = file.getCanonicalPath();
        this.timestamp = file.lastModified();
        this.size = file.length();
    }

    public FileMetadata(String filename, long timestamp, long size) {
        this.filename = filename;
        this.timestamp = timestamp;
        this.size = size;
    }

    public static FileMetadata deserialize(String serializedMetadata) {
        String[] parts = serializedMetadata.split(delim);

        String filename = parts[0];
        long timestamp = Long.parseLong(parts[1]);
        long size = Long.parseLong(parts[2]);

        return new FileMetadata(filename, timestamp, size);
    }

    public String serialize() {
        return filename + delim + timestamp + delim + size;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        FileMetadata an = (FileMetadata) o;
        return new EqualsBuilder()
                .append(filename, an.filename)
                .append(timestamp, an.timestamp)
                .append(size, an.size)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(643, 443)
                .append(filename)
                .append(timestamp)
                .append(size)
                .toHashCode();
    }

    public String getFilename() {
        return filename;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public long getSize() {
        return size;
    }
}

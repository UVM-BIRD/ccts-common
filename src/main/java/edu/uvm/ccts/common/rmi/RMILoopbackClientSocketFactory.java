/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.rmi;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.Socket;
import java.rmi.server.RMIClientSocketFactory;

/**
 * Created by mbstorer on 10/23/15.
 */
public class RMILoopbackClientSocketFactory implements RMIClientSocketFactory, Serializable {
    private static final Log log = LogFactory.getLog(RMILoopbackClientSocketFactory.class);

    private static final InetAddress addr = InetAddress.getLoopbackAddress();

    @Override
    public Socket createSocket(String host, int port) throws IOException {
        log.debug("creating socket to " + addr + " on port " + port);
        return new Socket(addr, port);
    }
}

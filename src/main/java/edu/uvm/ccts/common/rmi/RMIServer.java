/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by mstorer on 8/8/14.
 */
public class RMIServer {
    public static final String DEFAULT_HOST = null;     // null indicates localhost
    public static final int DEFAULT_COMM_PORT = 0;      // 0 indicates a random, anonymous port

    private String registryName;
    private int registryPort;
    private int commPort;
    private Remote remote;
    private RMIClientSocketFactory csf;
    private RMIServerSocketFactory ssf;


    public RMIServer(String registryName, int registryPort, Remote remote) throws Exception {
        this(registryName, registryPort, DEFAULT_COMM_PORT, remote, null, null);
    }

    public RMIServer(String registryName, int registryPort, int commPort, Remote remote) throws Exception {
        this(registryName, registryPort, commPort, remote, null, null);
    }

    public RMIServer(String registryName, int registryPort, Remote remote,
                     RMIClientSocketFactory csf, RMIServerSocketFactory ssf) throws Exception {
        this(registryName, registryPort, DEFAULT_COMM_PORT, remote, csf, ssf);
    }

    public RMIServer(String registryName, int registryPort, int commPort, Remote remote,
                     RMIClientSocketFactory csf, RMIServerSocketFactory ssf) throws Exception {
        this.registryName = registryName;
        this.registryPort = registryPort;
        this.commPort = commPort;
        this.remote = remote;
        this.csf = csf;
        this.ssf = ssf;
    }

    public boolean start() throws RemoteException {
        Registry registry = LocateRegistry.createRegistry(registryPort, csf, ssf);

        try {
            registry.lookup(registryName);
            return false;

        } catch (Exception e) {
            try {
                UnicastRemoteObject.unexportObject(remote, false);

            } catch (Exception e2) {
                // handle silently - was not exported to begin with
            }

            Remote stub = UnicastRemoteObject.exportObject(remote, commPort, csf, ssf);
            registry.rebind(registryName, stub);
            return true;
        }
    }

    public boolean stop() throws RemoteException {
        Registry registry;
        try {
            registry = LocateRegistry.getRegistry(DEFAULT_HOST, registryPort, csf);

        } catch (Exception e) {
            return false;
        }

        try {
            registry.unbind(registryName);
            return true;

        } catch (Exception e) {
            return false;
        }
    }
}

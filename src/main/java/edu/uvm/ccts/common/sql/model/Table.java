/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.sql.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by mstorer on 11/1/13.
 */
public class Table implements Comparable<Table> {
    private String name;
    private String alias;

    public Table(String name, String alias) {
        this.name = name == null ? "" : name;
        this.alias = alias == null ? "" : alias;
    }

    public Table(String name) {
        this.name = name == null ? "" : name;
        this.alias = "";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)                  return false;
        if (o == this)                  return true;
        if (o.getClass() != getClass()) return false;

        Table t = (Table) o;
        return new EqualsBuilder()
                .append(name, t.name)
                .append(alias, t.alias)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(557, 263)
                .append(name)
                .append(alias)
                .toHashCode();
    }

    @Override
    public int compareTo(Table t) {
        if (this == t) return 0;

        int x = name.compareTo(t.name);
        if (x != 0) return x;

        x = alias.compareTo(t.alias);
        if (x != 0) return x;

        assert this.equals(t) : "compareTo inconsistent with equals.";

        return 0;
    }

    public boolean hasNameOrAlias(String s) {
        return name.equalsIgnoreCase(s) || (alias != null && alias.equalsIgnoreCase(s));
    }

    public String getAliasOrName() {
        return ! alias.isEmpty() ? alias : name;
    }

    public String getName() {
        return name;
    }

    public String getAlias() {
        return alias;
    }
}

/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Created by JFormDesigner on Tue Sep 16 11:06:30 EDT 2014
 */

package edu.uvm.ccts.common.ui;

import edu.uvm.ccts.common.util.ResourceUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * @author Matthew Storer
 */
public class ConfirmDialog extends JDialog {
    private static final Log log = LogFactory.getLog(ConfirmDialog.class);

    private static final String ICON_FILE = "/question_mark.png";

    private boolean confirmed = false;

    public ConfirmDialog(Frame owner, String message) {
        super(owner);
        initComponents();
        setup(message);
    }

    public ConfirmDialog(Dialog owner, String message) {
        super(owner);
        initComponents();
        setup(message);
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    private void setup(String message) {
        try {
            BufferedImage bimg = ImageIO.read(ResourceUtil.getResourceAsStream(ICON_FILE));
            ImageIcon icon = new ImageIcon(bimg);
            lblIcon.setIcon(icon);

        } catch (IOException e) {
            log.error("caught " + e.getClass().getName() + " loading icon " + ICON_FILE + " - " + e.getMessage(), e);
            lblIcon.setVisible(false);
        }

        lblMessage.setText("<html>" + message + "</html>");

        noButton.grabFocus();
    }

    private void noButtonActionPerformed() {
        setVisible(false);
    }

    private void yesButtonActionPerformed() {
        confirmed = true;
        setVisible(false);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        lblIcon = new JLabel();
        lblMessage = new JLabel();
        buttonBar = new JPanel();
        noButton = new JButton();
        yesButton = new JButton();

        //======== this ========
        setTitle("Select a Response");
        setModal(true);
        setMinimumSize(new Dimension(350, 175));
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {

                //---- lblIcon ----
                lblIcon.setText("icon");
                lblIcon.setIcon(null);
                lblIcon.setPreferredSize(new Dimension(50, 50));

                //---- lblMessage ----
                lblMessage.setText("text");
                lblMessage.setVerticalAlignment(SwingConstants.TOP);

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(lblIcon, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(lblMessage, GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
                            .addContainerGap())
                );
                contentPanelLayout.setVerticalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addComponent(lblMessage, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addComponent(lblIcon, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, Short.MAX_VALUE)))
                            .addContainerGap())
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- noButton ----
                noButton.setText("No");
                noButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        noButtonActionPerformed();
                    }
                });
                buttonBar.add(noButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- yesButton ----
                yesButton.setText("Yes");
                yesButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        yesButtonActionPerformed();
                    }
                });
                buttonBar.add(yesButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        setSize(355, 180);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel lblIcon;
    private JLabel lblMessage;
    private JPanel buttonBar;
    private JButton noButton;
    private JButton yesButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}

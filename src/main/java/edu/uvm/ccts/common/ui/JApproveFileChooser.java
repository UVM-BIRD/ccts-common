/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.ui;

import edu.uvm.ccts.common.util.DialogUtil;

import javax.swing.*;
import java.io.File;

/**
 * Created by mstorer on 10/16/14.
 */
public class JApproveFileChooser extends JFileChooser {
    @Override
    public void approveSelection() {
        File f = getSelectedFile();

        if (f.exists() && getDialogType() == SAVE_DIALOG) {

            ConfirmDialog confirmDlg = new ConfirmDialog(DialogUtil.getParentDialog(this),
                    "The specified file already exists.  Would you like to overwrite it?");

            confirmDlg.setVisible(true);

            if (confirmDlg.isConfirmed()) {
                super.approveSelection();
            }

        } else {
            super.approveSelection();
        }
    }
}

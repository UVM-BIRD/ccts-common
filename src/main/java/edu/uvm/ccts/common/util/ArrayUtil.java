package edu.uvm.ccts.common.util;

/**
 * Created by mbstorer on 9/24/15.
 */
public class ArrayUtil {
    public static void wipe(byte[] arr) {
        if (arr != null) for (int i = 0; i < arr.length; i ++) arr[i] = 0;
    }
}

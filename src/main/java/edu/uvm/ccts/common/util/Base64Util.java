/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import org.apache.commons.net.util.Base64;

import java.io.*;

/**
 * Created by mstorer on 1/28/14.
 */
public class Base64Util {
    public static String serialize(Object obj) throws IOException {
        return Base64.encodeBase64String(ObjectUtil.serialize(obj), false);
    }

    public static Object deserialize(String base64String) throws IOException, ClassNotFoundException {
        return ObjectUtil.deserialize(Base64.decodeBase64(base64String));
    }
}

/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import java.awt.*;

/**
 * Created by mstorer on 10/7/14.
 */
public class ColorUtil {
    public static Color darken(Color c, int amount) {
        int r = Math.max(c.getRed() - amount, 0);
        int g = Math.max(c.getGreen() - amount, 0);
        int b = Math.max(c.getBlue() - amount, 0);
        return new Color(r, g, b);
    }

    public static Color lighten(Color c, int amount) {
        int r = Math.min(c.getRed() + amount, 255);
        int g = Math.min(c.getGreen() + amount, 255);
        int b = Math.min(c.getBlue() + amount, 255);
        return new Color(r, g, b);
    }

    public static Color foregroundForColor(Color c) {
        return getBrightness(c) < 130 ?
                Color.WHITE :
                Color.BLACK;

    }

    // lifted from http://tech.chitgoks.com/2010/07/27/check-if-color-is-dark-or-light-using-java/
    public static int getBrightness(Color c) {
        return (int) Math.sqrt(
                c.getRed() * c.getRed() * 0.241 +
                        c.getGreen() * c.getGreen() * 0.691 +
                        c.getBlue() * c.getBlue() * 0.068
        );
    }
}

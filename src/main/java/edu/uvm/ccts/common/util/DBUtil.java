/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import edu.uvm.ccts.common.db.DataSource;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by mstorer on 11/25/13.
 */
public class DBUtil {
    public static void executeUpdate(String sql, DataSource dataSource) throws SQLException {
        Statement stmt = null;

        try {
            stmt = dataSource.getConnection().createStatement();
            stmt.executeUpdate(sql);

        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {}
        }
    }

    public static void executeSelect(String sql, DataSource dataSource) throws SQLException {
        Statement stmt = null;

        try {
            stmt = dataSource.getConnection().createStatement();
            stmt.executeQuery(sql);

        } finally {
            try { if (stmt != null) stmt.close(); } catch (Exception e) {}
        }
    }
}

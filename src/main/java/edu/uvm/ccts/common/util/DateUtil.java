/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import edu.uvm.ccts.common.exceptions.UnhandledClassException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by mstorer on 7/22/14.
 */
public class DateUtil {
    private static final Pattern TimestampPattern = Pattern.compile("^[1-9]\\d{12}$");
    private static final Pattern DateOnlyPattern = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");
    private static final Pattern DatePatternSpace = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}\\s.*");
    private static final Pattern DatePatternT = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}T.*");
    private static final Pattern HourMinutePattern = Pattern.compile(".*\\d{2}:\\d{2}.*");
    private static final Pattern SecondPattern = Pattern.compile(".*\\d{2}:\\d{2}:\\d{2}.*");
    private static final Pattern MillisecondPattern = Pattern.compile(".*\\.\\d+.*");
    private static final Pattern TimezonePattern = Pattern.compile(".*([zZ]|(\\+|-)\\d{2}:\\d{2})$");


    public static Date castToDate(Object obj) throws ParseException {
        if      (obj == null)           return null;
        else if (obj instanceof Date)   return (Date) obj;
        else if (obj instanceof Long)   return new Date((Long) obj);
        else if (obj instanceof String) return parseDate((String) obj);
        else                            throw new UnhandledClassException("cannot parse " + obj.getClass().getName() + " to Date");
    }

    public static Date parseDate(String s) throws ParseException {
        if (TimestampPattern.matcher(s).matches()) {
            return new Date(Long.valueOf(s));
        }

        StringBuilder sb = new StringBuilder();

        if (DateOnlyPattern.matcher(s).matches()) {
            sb.append("yyyy-MM-dd");

        } else if (DatePatternT.matcher(s).matches()) {
            sb.append("yyyy-MM-dd'T'");

        } else if (DatePatternSpace.matcher(s).matches()) {     // dates in databases may be stored as strings.
            sb.append("yyyy-MM-dd ");                           // when they are, they will not use 'T'
        }

        if (HourMinutePattern.matcher(s).matches()) {
            sb.append("HH:mm");

            if (SecondPattern.matcher(s).matches()) {
                sb.append(":ss");

                if (MillisecondPattern.matcher(s).matches()) {
                    sb.append(".SSS");
                }
            }

            if (TimezonePattern.matcher(s).matches()) {
                sb.append("X");
            }
        }

        String format = sb.toString();
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        try {
            return sdf.parse(s);

        } catch (ParseException e) {
            throw new ParseException("couldn't parse '" + s + "' using format '" + format + "'", e.getErrorOffset());
        }
    }
}

/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import edu.uvm.ccts.common.ui.ConfirmDialog;
import edu.uvm.ccts.common.ui.ShowException;

import javax.swing.*;
import java.awt.*;

/**
 * Created by mstorer on 1/30/14.
 */
public class DialogUtil {
    public static void showErrorDialog(Component parent, String message) {
        showErrorDialog(parent, "Error", message);
    }

    public static void showErrorDialog(Component parent, String title, String message) {
        JOptionPane.showMessageDialog(parent, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public static void showInformationDialog(Component parent, String title, String message) {
        JOptionPane.showMessageDialog(parent, message, title, JOptionPane.INFORMATION_MESSAGE);
    }

    public static Boolean showConfirmDialog(Component parent, String message) {
        ConfirmDialog cDlg;
        if      (parent instanceof Frame)   cDlg = new ConfirmDialog((Frame) parent, message);
        else if (parent instanceof Dialog)  cDlg = new ConfirmDialog((Dialog) parent, message);
        else                                cDlg = new ConfirmDialog(getParentFrame(parent), message);

        cDlg.setVisible(true);

        return cDlg.isConfirmed();
    }

    public static Frame getParentFrame(Component c) {
        if      (c == null)             return null;
        else if (c instanceof Frame)    return (Frame) c;
        else {
            Component parent = c.getParent();
            while (parent != null) {
                if (parent instanceof Frame) break;
                parent = parent.getParent();
            }
            return (Frame) parent;
        }
    }

    public static Dialog getParentDialog(Component c) {
        if      (c == null)             return null;
        else if (c instanceof Dialog)   return (Dialog) c;
        else {
            Component parent = c.getParent();
            while (parent != null) {
                if (parent instanceof Dialog) break;
                parent = parent.getParent();
            }
            return (Dialog) parent;
        }
    }

    public static void showException(Frame parent, String title, Throwable t) {
        new ShowException(parent, title, t).setVisible(true);
    }

    public static void showException(Dialog parent, String title, Throwable t) {
        new ShowException(parent, title, t).setVisible(true);
    }
}

/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import edu.uvm.ccts.common.exceptions.ConfigurationException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.ArrayUtils;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by mbstorer on 8/11/15.
 */
public class EncryptionUtil {
    private static final byte[] DEFAULT_SALT = {123, -125, 24, 17, -48, -52, 120, 0};
    private static final byte[] DEFAULT_IV = {-1, -110, -93, -44, -107, 102, -94, -21, 73, -30, 91, -6, -35, -7, 41, 74};

    public static byte[] encrypt(byte[] bytes, byte[] key) throws ConfigurationException {
        return encrypt(bytes, key, DEFAULT_SALT, DEFAULT_IV);
    }

    public static byte[] encrypt(byte[] bytes, byte[] key, byte[] salt) throws ConfigurationException {
        return encrypt(bytes, key, salt, DEFAULT_IV);
    }

    public static byte[] encrypt(byte[] bytes, byte[] key, byte[] salt, byte[] iv) throws ConfigurationException {
        try {
            Cipher cipher = buildCipher(Cipher.ENCRYPT_MODE, key, salt, iv);
            return cipher.doFinal(bytes);

        } catch (Exception e) {
            throw new ConfigurationException(e.getMessage(), e);
        }
    }

    public static byte[] decrypt(byte[] bytes, byte[] key) throws ConfigurationException {
        return decrypt(bytes, key, DEFAULT_SALT, DEFAULT_IV);
    }

    public static byte[] decrypt(byte[] bytes, byte[] key, byte[] salt) throws ConfigurationException {
        return decrypt(bytes, key, salt, DEFAULT_IV);
    }

    public static byte[] decrypt(byte[] bytes, byte[] key, byte[] salt, byte[] iv) throws ConfigurationException {
        try {
            Cipher cipher = buildCipher(Cipher.DECRYPT_MODE, key, salt, iv);
            return cipher.doFinal(bytes);

        } catch (Exception e) {
            throw new ConfigurationException(e.getMessage(), e);
        }
    }

    private static Cipher buildCipher(int mode, byte[] key, byte[] salt, byte[] iv) throws NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException, InvalidAlgorithmParameterException {

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] md5 = DigestUtils.md5(ArrayUtils.addAll(key, salt));
        SecretKeySpec secretKey = new SecretKeySpec(md5, "AES");
        IvParameterSpec ivspec = new IvParameterSpec(iv);
        cipher.init(mode, secretKey, ivspec);
        return cipher;
    }
}

/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Created by mstorer on 5/13/14.
 */
public class HashUtil {
    public static String buildKey(Object ... objs) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < objs.length - 1; i ++) {
            sb.append(String.valueOf(objs[i])).append("\u0000;\u0000");
        }
        sb.append(String.valueOf(objs[objs.length - 1]));

        byte[] bytes = DigestUtils.md5(sb.toString().getBytes());

        return new BigInteger(1, bytes).toString(16);
    }

    public static String buildChecksum(File file) throws IOException {
        return DigestUtils.md5Hex(new FileInputStream(file));
    }
}

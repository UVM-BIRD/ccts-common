/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by mstorer on 6/3/15.
 */
public class JarUtil {
    public static String getJarPath(Class cls) throws URISyntaxException, IOException {
        return new File(cls.getProtectionDomain().getCodeSource().getLocation().toURI()).getCanonicalPath();
    }

    public static String getJarDir(Class cls) throws IOException, URISyntaxException {
        return FileUtil.getPathPart(getJarPath(cls));
    }
}

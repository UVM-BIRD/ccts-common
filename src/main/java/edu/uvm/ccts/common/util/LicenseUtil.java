/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import edu.uvm.ccts.common.ui.licensing.LicenseForm;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 6/2/15.
 */
public class LicenseUtil {
    private static final Log log = LogFactory.getLog(LicenseUtil.class);

    private static String LICENSE_DATA_FILE;
    static {
        try {
            LICENSE_DATA_FILE = FileUtil.buildPath(JarUtil.getJarDir(LicenseUtil.class), ".license-data");
        } catch (Exception e) {
            LICENSE_DATA_FILE = ".license-data";
        }
    }

    @SuppressWarnings("unchecked")
    public static void requireLicenseAcceptance(Class cls, String title, String licenseResourceFile) {
        boolean showLicense = true;
        List<String> acceptedList = null;

        if (FileUtil.exists(LICENSE_DATA_FILE)) {
            try {
                acceptedList = (List<String>) FileUtil.deserializeFromFile(LICENSE_DATA_FILE);
                showLicense = ! acceptedList.contains(cls.getCanonicalName());

            } catch (Exception e) {
                log.warn("caught " + e.getClass().getName() + " deserializing license file.  will require reacceptance.");
                FileUtil.delete(LICENSE_DATA_FILE);
            }
        }

        if (showLicense) {
            try {
                LicenseForm licenseForm = new LicenseForm(title, licenseResourceFile);
                licenseForm.setVisible(true);

                if (licenseForm.isAccepted()) {
                    if (acceptedList == null) acceptedList = new ArrayList<String>();
                    acceptedList.add(cls.getCanonicalName());
                    FileUtil.serializeToFile(LICENSE_DATA_FILE, acceptedList);

                } else {
                    log.info("License declined.  Exiting...");
                    System.exit(0);
                }

            } catch (IOException e) {
                log.error("caught " + e.getClass().getName() + " showing license - " + e.getMessage(), e);
            }
        }
    }
}

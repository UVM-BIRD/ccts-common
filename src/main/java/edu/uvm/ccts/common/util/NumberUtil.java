/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

/**
 * Created by mstorer on 2/12/14.
 */
public class NumberUtil {
    public static Integer convertToInteger(Object obj) {
        if (obj == null) return null;

        if (obj instanceof Number) {
            return ((Number) obj).intValue();
        }

        if (obj instanceof String) {
            String s = (String) obj;

            if (s.indexOf('.') >= 0) {
                return Math.round(Float.parseFloat(s));

            } else {
                return Integer.parseInt(s);
            }
        }

        throw new ClassCastException(obj.getClass().getName() + " cannot be cast to " + Integer.class.getName());
    }

    public static Float convertToFloat(Object obj) {
        if (obj == null) return null;

        if (obj instanceof Number) {
            return ((Number) obj).floatValue();
        }

        if (obj instanceof String) {
            return Float.parseFloat((String) obj);
        }

        throw new ClassCastException(obj.getClass().getName() + " cannot be cast to " + Float.class.getName());
    }

    public static boolean in(Number value, Number ... numbers) {
        if (value == null) return false;
        for (Number n : numbers) {
            if (value.equals(n)) return true;
        }
        return false;
    }

    public static boolean inRange(Integer value, String range, boolean allowNull) {
        if (value == null) return allowNull;

        for (String rangePart : range.split("\\s*,\\s*")) {
            if (rangePart.contains("-")) {
                String[] minMax = rangePart.split("\\s*-\\s*");

                int min = Integer.parseInt(minMax[0]);
                int max = Integer.parseInt(minMax[1]);
                if (value >= min && value <= max) {
                    return true;
                }

            } else {
                int i = Integer.parseInt(rangePart);
                if (value.equals(i)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean inRange(Float value, String range, boolean allowNull) {
        if (value == null) return allowNull;

        for (String rangePart : range.split("\\s*,\\s*")) {
            if (rangePart.contains("-")) {
                String[] minMax = rangePart.split("\\s*-\\s*");

                float min = Float.parseFloat(minMax[0]);
                float max = Float.parseFloat(minMax[1]);
                if (value >= min && value <= max) {
                    return true;
                }

            } else {
                float i = Float.parseFloat(rangePart);
                if (value.equals(i)) {
                    return true;
                }
            }
        }

        return false;
    }
}

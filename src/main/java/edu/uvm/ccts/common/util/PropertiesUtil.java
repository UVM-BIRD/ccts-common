/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import java.io.*;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Created by mstorer on 11/25/13.
 */
public class PropertiesUtil {
    public static Properties filter(String str, Properties props) {
        Properties filtered = new Properties();
        for (String name : props.stringPropertyNames()) {
            if (name.toLowerCase().startsWith(str.toLowerCase())) {
                filtered.put(name, props.getProperty(name));
            }
        }
        return filtered;
    }

    public static Properties loadFromResource(String path) throws IOException {
        Properties props = new Properties();
        props.load(new ByteArrayInputStream(ResourceUtil.readResource(path)));
        return props;
    }

    public static Properties loadFromFile(File file) throws IOException {
        Properties props = new Properties();
        props.load(new FileInputStream(file));
        return props;
    }

    public static Properties copy(Properties src) {
        Properties dest = new Properties();

        Enumeration en = src.propertyNames();
        while (en.hasMoreElements()) {
            String key = String.valueOf(en.nextElement());
            String value = src.getProperty(key);
            dest.setProperty(key, value);
        }

        return dest;
    }
}

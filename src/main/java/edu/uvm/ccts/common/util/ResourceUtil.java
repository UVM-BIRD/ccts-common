/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import java.io.*;

/**
 * Created by mstorer on 2/28/14.
 */
public class ResourceUtil {
    public static byte[] readResource(String resource) throws IOException {
        InputStream in = null;
        ByteArrayOutputStream out = null;

        try {
            in = getResourceAsStream(resource);
            if (in != null) {
                out = new ByteArrayOutputStream();
                byte[] buf = new byte[4096];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }

        } finally {
            if (in != null) try { in.close(); } catch (Exception e) { }
            if (out != null) try { out.flush(); } catch (Exception e) { }
            if (out != null) try { out.close(); } catch (Exception e) { }
        }

        return out != null ? out.toByteArray() : null;
    }

    public static InputStream getResourceAsStream(String resource) {
        return ResourceUtil.class.getResourceAsStream(resource);
    }
}

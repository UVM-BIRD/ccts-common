/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 12/12/13.
 */
public class TimeUtil {
    public static String formatMsToHMS(long ms) {
        int secondsInHour = 60 * 60;
        int secondsInMinute = 60;

        int seconds = (int) (ms / 1000);

        int hours = (int) ((float) seconds / secondsInHour);
        seconds -= (hours * secondsInHour);

        int minutes = (int) ((float) seconds / secondsInMinute);
        seconds -= (minutes * secondsInMinute);

        List<String> list = new ArrayList<String>();
        if (hours == 1)     list.add(hours + " hour");
        else if (hours > 1) list.add(hours + " hours");

        if (minutes == 1)       list.add(minutes + " minute");
        else if (minutes > 1)   list.add(minutes + " minutes");

        if (seconds == 1)       list.add(seconds + " second");
        else if (seconds > 1)   list.add(seconds + " seconds");

        return list.isEmpty() ? "0 seconds" : StringUtils.join(list, ", ");
    }
}

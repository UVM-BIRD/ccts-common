/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of CCTS Common.
 *
 * CCTS Common is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CCTS Common is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CCTS Common.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.common.util;

import electric.xml.Element;
import electric.xml.XPath;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by mstorer on 1/17/14.
 */
public class XmlUtil {
    public static void serialize(Object obj, File file) throws JAXBException {
        JAXBContext ctx = JAXBContext.newInstance(obj.getClass());
        Marshaller marshaller = ctx.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        marshaller.marshal(obj, file);
    }

    public static Object deserialize(Class klass, File file) throws JAXBException {
        JAXBContext ctx = JAXBContext.newInstance(klass);

        Unmarshaller unmarshaller = ctx.createUnmarshaller();
        return unmarshaller.unmarshal(file);
    }

    public static String getText(Element element, String subElementName) {
        if (element != null) {
            Element subElement = element.getElement(new XPath(subElementName));
            if (subElement != null) {
                return subElement.getTrimTextString();
            }
        }
        return null;
    }
}
